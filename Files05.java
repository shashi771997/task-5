package files;
import java.io.*;

/************************************************************************
 * This program sorts the lines of a file so that the 
 * shortest lines are output last.
 *  
 * @author Shashimal, 2021
 ************************************************************************/
public class Files05 {
  public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("E:/SE TOP UP - "
    		+ "CINEC/Advanced Software Engineering Topics -"
    		+ " 6CS002/Task 5 - Final Task"
    		+ "/data/wolf-fox.txt"));

    r.lines().sorted((a, b) -> {
      if (a.length() == b.length())
        return 0;
      if (a.length() < b.length())
        return 1;
      return -1;
    }).forEach(l -> System.out.println(l));
    r.close();
  }
}

