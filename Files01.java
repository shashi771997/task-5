package files;

import java.io.*;

/**********************************************************************
 * This code shows how to process all lines of a text file.
 * 
 * @author Shashimal, 2021
 **********************************************************************/

public class Files01 {

  public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("E:/SE TOP UP - "
    		+ "CINEC/Advanced Software Engineering Topics -"
    		+ " 6CS002/Task 5 - Final Task"
    		+ "/data/wolf-fox.txt"));

    r.lines().forEach(l -> System.out.println(l));

    r.close();
  }

}

