package files;

import java.io.*;

/**************************************************************************
 * This program reads a text file and only prints the lines 
 * containing the word "his".
 * 
 * @author Shashimal, 2021
 **************************************************************************/

public class Files03 {

  public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("E:/SE TOP UP - "
    		+ "CINEC/Advanced Software Engineering Topics -"
    		+ " 6CS002/Task 5 - Final Task"
    		+ "/data/wolf-fox.txt"));

    r.lines().filter(l -> l.contains("his"))
        .forEach(l -> System.out.println(l));

    r.close();
  }

}

