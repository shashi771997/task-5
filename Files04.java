package files;
import java.io.*;

/*****************************************************************
 * This program uses a map to convert a text file to upper case.
 * 
 * @author Shashimal, 2021
 ******************************************************************/

public class Files04 {

  public static void main(String[] args) throws Exception {
    BufferedReader r  = 
        new BufferedReader(new FileReader("E:/SE TOP UP - "
        		+ "CINEC/Advanced Software Engineering Topics -"
        		+ " 6CS002/Task 5 - Final Task"
        		+ "/data/wolf-fox.txt"));
    
    r.lines()
     .map(l -> l.toUpperCase())
     .forEach(l -> System.out.println(l));
    
    r.close();
  }
  
}


