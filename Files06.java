package files;
import java.io.*;

/************************************************************
 * This program prints the contents of a file on a single 
 * line. It is demonstrating a reduce operation and uses 
 * different syntax to access the concat method of the 
 * String class.
 * 
 * @author Shashimal, 2021
 ************************************************************/

public class Files06 {

  public static void main(String[] args) throws Exception {
    BufferedReader r  = 
       new BufferedReader(new FileReader("E:/SE TOP UP - "
       		+ "CINEC/Advanced Software Engineering Topics -"
       		+ " 6CS002/Task 5 - Final Task"
       		+ "/data/wolf-fox.txt"));

    System.out.println(r.lines().reduce("", String::concat));
    
    r.close();
  }
}

