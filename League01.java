package ipl;

import java.util.Arrays;
import java.util.List;

/**************************************************************
 * This example demonstrates processing a list 
 * of integers with a stream.     
 * 
 * @author Shashimal Maduranga Alwis, 2021
 **************************************************************/


public class League01 {
  public static void main(String[] args) {
    List<Council> table = Arrays.asList(
        new Council(1, "Royal Challengers ", 122, 160, 80, 55, 
        		191, 260, 221, 175, 99, 89, 200, 192),
        new Council(2, "Mumbai Indians", 220, 160, 100, 86, 225, 
        		314, 219, 172, 243, 99, 192, 275),
        new Council(3, "Chennai Super Kings", 122, 159, 201, 96, 
        		253, 221, 132, 370, 239, 40, 92, 168),
        new Council(4, "Delhi Capitals", 122, 149, 101, 79, 364,
        		218, 146, 270, 240, 155, 85, 268),
        new Council(5, "Exeter Chiefs", 122, 140, 200, 89, 263, 
        		237, 229, 270, 246, 59, 177, 168),
        new Council(6, "Punjab Lions", 122, 119, 192, 99, 132, 
        		327, 245, 277, 254, 99, 49, 161),
        new Council(7, "Kolkata Riders", 322, 211, 90, 311, 
        		297, 182, 159, 162, 254, 69, 48, 154),
        new Council(8, "Rajasthan Royals", 122, 109, 90, 312, 
        		244, 114, 270, 145, 150, 40, 50, 49),
        new Council(9, "Sunrisers Hyderabad", 222, 95, 81, 212, 
        		253, 275, 122, 153, 161, 49, 69, 48),
        new Council(10, "Deccan Chargers", 122, 79, 91, 114, 242, 
        		178, 136, 146, 157, 49, 96, 49),
        new Council(11, "Kings xi Punjab", 192, 95, 91, 96, 175, 
        		245, 170, 59, 161, 49, 88, 340),
        new Council(12, "Gujarat Lions", 221, 90, 70, 122, 123, 
        		321, 98, 229, 47, 100, 90, 91));

     table.forEach(x -> System.out.println(x));
  }

}
