package files;

import java.io.*;

/***********************************************************
 * This program counts the number of lines in a text file.
 * 
 * @author Shashimal, 2021
 ************************************************************/

public class Files02 {

  public static void main(String[] args) throws Exception {
    BufferedReader r  = 
      new BufferedReader(new FileReader("E:/SE TOP UP - "
      		+ "CINEC/Advanced Software Engineering Topics -"
      		+ " 6CS002/Task 5 - Final Task"
      		+ "/data/wolf-fox.txt"));

    System.out.println(r.lines().count());

    r.close();
  }

}


