package files;
import java.io.*;
import java.util.*;
/*************************************************************
 * This program demonstrates using Optional String.
 * 
 * @author Shashimal, 2021
 **************************************************************/
public class Files07 {
  public static void main(String[] args) throws Exception {
    BufferedReader r  = 
      new BufferedReader(new FileReader("E:/SE TOP UP - "
      		+ "CINEC/Advanced Software Engineering Topics -"
      		+ " 6CS002/Task 5 - Final Task"
      		+ "/data/wolf-fox.txt"));
    Optional <String >result = 
      r.lines()
       .reduce((left, right) -> left.concat(" ".concat(right)));
    
    if(result.isPresent())
      System.out.println("result is " + result.get());
    else
      System.out.println("result not present");
    r.close();
  }
}

