package files;
import java.io.*;
import java.util.*;
import java.util.stream.*;
/****************************************************************
 * This example uses a collector to combine all the lines of a 
 * file into a list.
 * 
 * @author Shashimal, 2021
 ****************************************************************/
public class Files08 {
  public static void main(String[] args) throws Exception {
    BufferedReader r  = 
        new BufferedReader(new FileReader("E:/SE TOP UP - "
        		+ "CINEC/Advanced Software Engineering Topics -"
        		+ " 6CS002/Task 5 - Final Task"
        		+ "/data/wolf-fox.txt"));

    List<String> l = r.lines().collect(Collectors.toList());
    
    for(String line: l){
      System.out.println(line);
    }    
    r.close();
  }
}

